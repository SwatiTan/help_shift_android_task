package google.sqlitedemo;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private StudentHelper studentHelper;
    private ListView studentListView;
    private SimpleCursorAdapter simpleCursorAdapter;
    private StudentCursorAdapter studentCursorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button insertRecordButton = (Button) findViewById
                (R.id.insertRecordButton);
        Button readRecordButton = (Button) findViewById
                (R.id.readRecordButton);
        Button readCustomRecordButton = (Button) findViewById
                (R.id.readCustomRecordButton);
        Button updateRecordButton = (Button) findViewById
                (R.id.updateRecordButton);
        Button deleteRecordButton = (Button) findViewById
                (R.id.deleteRecordButton);

        studentListView = (ListView) findViewById(R.id.studentListView);

        insertRecordButton.setOnClickListener(this);
        readRecordButton.setOnClickListener(this);
        readCustomRecordButton.setOnClickListener(this);
        updateRecordButton.setOnClickListener(this);
        deleteRecordButton.setOnClickListener(this);

        studentHelper = new StudentHelper(this);
        studentHelper.open();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.insertRecordButton:
                insertRecord();
                break;
            case R.id.readRecordButton:
                readRecord();
                break;
            case R.id.readCustomRecordButton:
                readCustomRecord();
                break;
            case R.id.updateRecordButton:
                updateRecord();
                break;
            case R.id.deleteRecordButton:
                deleteRecord();
                break;
        }
    }

    private void insertRecord() {
        studentHelper.addStudent("Vipul", 27, "Thane");
        studentHelper.addStudent("Vinay", 29, "Mulund");
        studentHelper.addStudent("Vishal", 32, "Powai");

    }

    private void readRecord() {
        Cursor cursor = studentHelper.readStudentForListView();
        simpleCursorAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                cursor,
                new String[]{StudentHelper.COLUMN_NAME, StudentHelper.COLUMN_LOCATION},
                new int[]{android.R.id.text1, android.R.id.text2},
                0);
        studentListView.setAdapter(simpleCursorAdapter);
    }

    private void readCustomRecord() {
        Cursor cursor = studentHelper.readStudentForListView();
        studentCursorAdapter = new StudentCursorAdapter(this, cursor, 0);
        studentListView.setAdapter(studentCursorAdapter);
    }

    private void updateRecord() {
        studentHelper.updateStudent();
        readRecord();
    }

    private void deleteRecord() {
        studentHelper.delete();readRecord();
    }
}
