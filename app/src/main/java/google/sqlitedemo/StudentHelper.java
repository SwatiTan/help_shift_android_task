package google.sqlitedemo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;


public class StudentHelper {
    public static final String DATABASE_NAME = "students.db";
    public static final int DATABASE_VERSION = 1;
    public static final String TABLE_NAME = "students";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_AGE = "age";
    public static final String COLUMN_LOCATION = "location";

    private static final String TABLE_CREATION_QUERY = "create table " + TABLE_NAME + "(" +
            COLUMN_ID + " integer primary key autoincrement," +
            COLUMN_NAME + " text," +
            COLUMN_AGE + " integer," +
            COLUMN_LOCATION + " text);";

    private DBHelper dbHelper;
    private SQLiteDatabase sqLiteDatabase;
    private Context context;

    public StudentHelper(Context context) {
        this.context = context;
        dbHelper = new DBHelper(context);
    }

    public void open() {
        sqLiteDatabase = dbHelper.getWritableDatabase();
    }

    public void addStudent(String name, int age, String location) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME, name);
        contentValues.put(COLUMN_AGE, age);
        contentValues.put(COLUMN_LOCATION, location);
        long rowId = sqLiteDatabase.insert(TABLE_NAME, null, contentValues);
        if (rowId != -1) {
            Toast.makeText(context, "Insert Success " + rowId, Toast.LENGTH_SHORT).show();
        }
    }

    public void readStudent() {
        Cursor cursor = sqLiteDatabase.query(TABLE_NAME, null,
                COLUMN_NAME + "=? AND " + COLUMN_AGE + "=?", new String[]{"Vishal", "32"}, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                String name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
                int age = cursor.getInt(cursor.getColumnIndex(COLUMN_AGE));
                String location = cursor.getString(cursor.getColumnIndex(COLUMN_LOCATION));

                Toast.makeText(context, name + ":" + age + ":" + location, Toast.LENGTH_SHORT).
                        show();

            } while (cursor.moveToNext());
        }

        if (cursor != null) {
            cursor.close();
        }

    }

    public Cursor readStudentForListView() {
        return sqLiteDatabase.
                query(TABLE_NAME, null, null, null, null, null, null);
    }

    public void updateStudent() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_LOCATION, "Dadar");
        int numRowsUpdated = sqLiteDatabase.update(TABLE_NAME, contentValues,
                COLUMN_NAME + "=? AND " + COLUMN_AGE + "=?", new String[]{"Vinay", "29"});

        Toast.makeText(context, "Updated " + numRowsUpdated + "row(s)", Toast.LENGTH_SHORT)
                .show();
    }

    public void delete() {
        int numRowsDeleted = sqLiteDatabase.delete(TABLE_NAME,
                COLUMN_NAME + "=? AND " + COLUMN_AGE + "=?", new String[]{"Vinay", "29"});
        Toast.makeText(context, "Deleted " + numRowsDeleted + "row(s)", Toast.LENGTH_SHORT)
                .show();

    }

    public void close() {
        sqLiteDatabase.close();
    }


    class DBHelper extends SQLiteOpenHelper {

        // creating database
        public DBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        // creating tables
        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(TABLE_CREATION_QUERY);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            //
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            //Log.w("tables dropped", "Onupgrade");
            //Log.w("tables dropped2", "Onupgrade");
            onCreate(db);
        }
    }
}
