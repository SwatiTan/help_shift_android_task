package google.sqlitedemo;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class StudentCursorAdapter extends CursorAdapter {

    public StudentCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.row_student_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        String name = cursor.getString(cursor.getColumnIndex(StudentHelper.COLUMN_NAME));
        int age = cursor.getInt(cursor.getColumnIndex(StudentHelper.COLUMN_AGE));
        String location = cursor.getString(cursor.getColumnIndex(StudentHelper.COLUMN_LOCATION));

        TextView studentName = (TextView) view.findViewById(R.id.studentName);
        TextView studentAge = (TextView) view.findViewById(R.id.studentAge);
        TextView studentLocation = (TextView) view.findViewById(R.id.studentLocation);

        studentName.setText(name);
        studentAge.setText(String.valueOf(age));
        studentLocation.setText(String.valueOf(location));
    }
}
